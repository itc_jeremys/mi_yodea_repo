# Imports
from urllib.request import urlopen
from bs4 import BeautifulSoup as Soup
from contextlib import suppress
import json
import mysql
import mysql.connector
import logging
import datetime
from datetime import timedelta
import time
import pandas as pd
import multiprocessing
from typing import Union
import re
import click

# URL constants
DOMAIN_NAME = "https://judaism.stackexchange.com"
QUESTIONS_URL = DOMAIN_NAME + "/questions?pagesize=50&sort=newest"
QUESTION_PREFIX = DOMAIN_NAME + '/questions/'
USER_PREFIX = DOMAIN_NAME + '/users/'
API_URL_TEMPLATE = 'https://www.hebcal.com/converter/?cfg=json&gd=%d&gm=%m&gy=%Y'
FILE_PATH_MAP = "mapping_parashiot.csv"

# Interval (s) between two page scraps - to avoid banning of IP
SCRAPE_INTERVAL = 0.6

# Logging
LOGFILE = 'scrape.log'
logging_enabled = True
printing_enabled = True
lastscrapefile = 'lastpagescrapped.txt'

# Init global variables
questions_list = []
users_list = []
tags_list = []
parashiot_list = []
last_question_page = 0

# DATABASE VARIABLES
database = 'mi_yodea'
mysql_username = 'python'
mysql_password = 'pass'
cnx = mysql.connector.connect(user=mysql_username, passwd=mysql_password)
cursor = cnx.cursor()


# This class offers colors when printing stuff in terminal
class TerminalColors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def enable_dev_env():
    """Replaces production variables with their dev environment counterparts"""
    global LOGFILE, database, lastscrapefile, printing_enabled
    database += '_dev'
    lastscrapefile = 'lastpagescrapped_dev.txt'


def output(message, severity='Info'):
    """Output function - prints and saves to logfile if enabled"""
    global logging_enabled, printing_enabled

    if printing_enabled:
        print(message)

    if logging_enabled:
        for val in TerminalColors.__dict__.keys():
            if val[:2] != '__':
                message = message.replace(getattr(TerminalColors, val), '')
        if severity == 'Info':
            logging.info('%s:\t%s' % (str(datetime.datetime.now()), message))
        elif severity == 'Error':
            logging.error(message)


def run_query(sql, no_results=False, verbose=False):
    results = None
    if verbose:
        output("Running Query: '%s'" % sql)

    try:
        # Execute the SQL command
        if no_results:
            cursor.execute(sql)
            cnx.commit()
            if verbose:
                output('Success - ' + str(cursor.rowcount) + ' rows affected')
        else:
            cursor.execute(sql)
            # Fetch all the rows in a list of lists.
            results = cursor.fetchall()
            # results are in an array containing the content of your query.
            # Use it as you wish ...

    except mysql.connector.Error as err:
        output(TerminalColors.FAIL + "ERROR: unable to fecth data. ##{}".format(err) + TerminalColors.ENDC, 'Error')
        output(TerminalColors.FAIL + "While running query: '%s' '%s'" % (sql, TerminalColors.ENDC), 'Error')
        return

    return results


def sql_create_db_if_doesnt_exist():
    """Creates the db """
    global database
    # run_query('DROP DATABASE IF EXISTS %s;' % database, True)
    run_query('CREATE DATABASE IF NOT EXISTS %s CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;' % database, True)
    run_query('USE %s;' % database, True)

    # USERS
    query = """
            CREATE TABLE IF NOT EXISTS Users (
                user_id MEDIUMINT(7) UNSIGNED NOT NULL AUTO_INCREMENT, 
                web_user_id MEDIUMINT(7) UNSIGNED NOT NULL, 
                url TEXT, 
                name varchar(30), 
                is_moderator BIT(1), 
                intro TEXT, 
                picture_url TEXT, 
                location varchar(100), 
                registration_date DATETIME, 
                nb_answers SMALLINT(6) UNSIGNED, 
                nb_questions SMALLINT(6) UNSIGNED, 
                nb_people_reached MEDIUMINT(7) UNSIGNED, 
                nb_profile_view MEDIUMINT(7) UNSIGNED, 
                reputation MEDIUMINT(7), 
                nb_gold_badges SMALLINT(6) UNSIGNED, 
                nb_silver_badges SMALLINT(6) UNSIGNED, 
                nb_bronze_badges SMALLINT(6) UNSIGNED, 
                PRIMARY KEY (user_id), 
                UNIQUE KEY(web_user_id));
            """
    run_query(query, True)

    # Unknown User
    query = """
            INSERT IGNORE INTO Users(
                web_user_id, 
                url, name, 
                is_moderator, 
                intro, 
                picture_url, 
                location, 
                registration_date, 
                nb_answers, 
                nb_questions, 
                nb_people_reached, 
                nb_profile_view, 
                reputation, 
                nb_gold_badges, 
                nb_silver_badges, 
                nb_bronze_badges) 
            VALUES(0, '', 'Unknown User', 0, 'DB Entry for unknown user', '', 'somewhere', 
                    '2000-01-01', 0, 0, 0, 0, 0, 0, 0, 0);
            """
    run_query(query, True)

    # TAGS
    query = """
            CREATE TABLE IF NOT EXISTS Tags (
                tag_id SMALLINT(4) UNSIGNED NOT NULL AUTO_INCREMENT, 
                label varchar(150),
                PRIMARY KEY (tag_id), 
                UNIQUE KEY (label));
            """
    run_query(query, True)

    # PARASHA-TAG MAPPING
    query = """
            CREATE TABLE IF NOT EXISTS Parasha (
                parasha_id SMALLINT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
                parasha_name varchar(150),
                taglabel varchar(150),
                PRIMARY KEY (parasha_id), 
                UNIQUE KEY (parasha_name));
            """
    run_query(query, True)

    fill_parasha_mapping()

    # QUESTIONS
    query = """
            CREATE TABLE IF NOT EXISTS Questions (
                question_id MEDIUMINT(6) UNSIGNED NOT NULL AUTO_INCREMENT,
                web_question_id MEDIUMINT(7) UNSIGNED NOT NULL,
                web_user_id MEDIUMINT(7) UNSIGNED, 
                url TEXT, 
                title TEXT, 
                question_text TEXT, 
                ask_date DATETIME, 
                weekly_parasha varchar(150),
                nb_views MEDIUMINT(7) UNSIGNED, 
                votes_count MEDIUMINT(7), 
                nb_favorites MEDIUMINT(7) UNSIGNED, 
                last_activity DATETIME, 
                PRIMARY KEY (question_id), 
                UNIQUE KEY(web_question_id), 
                FOREIGN KEY (web_user_id) REFERENCES Users(web_user_id),
                FOREIGN KEY (weekly_parasha) REFERENCES Parasha(parasha_name));
            """
    run_query(query, True)

    # QUESTIONSTAGS
    query = """
            CREATE TABLE IF NOT EXISTS QuestionsTags (
                qt_id MEDIUMINT(6) UNSIGNED NOT NULL AUTO_INCREMENT, 
                tag_label varchar(150), 
                question_web_id MEDIUMINT(7) UNSIGNED NOT NULL, 
                PRIMARY KEY (qt_id), 
                FOREIGN KEY (tag_label) REFERENCES Tags(label), 
                FOREIGN KEY (question_web_id) REFERENCES Questions(web_question_id));
            """
    run_query(query, True)

    # ANSWERS
    query = """
            CREATE TABLE IF NOT EXISTS Answers (
                answer_id MEDIUMINT(6) UNSIGNED NOT NULL AUTO_INCREMENT, 
                web_answer_id MEDIUMINT(7) UNSIGNED, 
                web_question_id MEDIUMINT(7) UNSIGNED NOT NULL, 
                web_user_id MEDIUMINT(7) UNSIGNED, 
                answer_text TEXT, 
                answer_date DATETIME, 
                votes_count MEDIUMINT(7), 
                accepted BIT(1),   
                PRIMARY KEY (answer_id), 
                FOREIGN KEY (web_question_id) REFERENCES Questions(web_question_id), 
                FOREIGN KEY (web_user_id) REFERENCES Users(web_user_id));
            """
    run_query(query, True)


def get_last_data_from_db():
    """Retrieve a list of questions, users and tags in order not to scrape twice the same page"""
    global questions_list, users_list, tags_list

    questions_list = [question[0] for question in run_query('SELECT web_question_id '
                                                            'FROM Questions ORDER BY question_id DESC;')]
    users_list = [user[0] for user in run_query('SELECT web_user_id FROM Users ORDER BY user_id DESC;')]
    tags_list = [label[0] for label in run_query('SELECT label FROM Tags ORDER BY tag_id DESC;')]


class Question(object):
    def __init__(self, question_id):
        global questions_list, parashiot_list

        # Update the question list
        questions_list.append(question_id)

        self.question_id = question_id
        self.url = QUESTION_PREFIX + str(question_id)

        try:
            # Scrape the containers
            question_details_soup = get_webpage_source(self.url)
            question_container = question_details_soup.find('div', class_='question')

            with suppress(AttributeError):
                tags_container = question_container.find('div', class_='post-taglist').find_all('a', class_='post-tag')
            question_stats = question_details_soup.find('div', class_='module question-stats')

            # Parse question details
            self.title = question_details_soup.find('a', class_='question-hyperlink').get_text()
            self.text = question_container.find('div', class_='post-text').get_text().strip()

            with suppress(Exception):
                self.tags = [tag.get_text() for tag in tags_container]

            # Parse stats
            self.ask_date = str(question_stats.find_all('p', class_='label-key')[1].attrs['title'])[:-1]

            self.weekly_parasha = None
            with suppress(Exception):
                weekly_parasha = parashat_hashavua(self.ask_date)
                if weekly_parasha in parashiot_list:
                    self.weekly_parasha = weekly_parasha

            self.view_count = int(question_stats.find_all('p', class_='label-key')[3]
                                  .get_text().strip().split()[0].replace(',', ''))
            self.vote_count = int(question_container.find('span', class_='vote-count-post').get_text().replace(',', ''))
            self.favorite_count = int('0' + question_container.find('div', class_='favoritecount').get_text())

            # Parse original asker's info
            self.user_id = None
            with suppress(AttributeError):
                owner_soup = question_details_soup.find('div', class_='owner').find('div', class_='user-details')
                user_url = DOMAIN_NAME + owner_soup.find('a').attrs['href']
                self.user_id = int(re.search('[\w.-]+/users/([0-9]+)/.*', user_url).group(1))

            self.last_activity = ""
            with suppress(AttributeError):
                self.last_activity = str(question_stats.find('a', class_='lastactivity-link').attrs['title'])[:-1]

            if self.last_activity == '':
                self.last_activity = self.ask_date

            # Parse Answers
            with suppress(AttributeError):
                self.answers = []
                answers_soup_list = question_details_soup.find_all('div', class_='answer')
                for answer in answers_soup_list:
                    with suppress(Exception):
                        self.answers.append(Answer(answer, self.question_id))

            output('Question:  %-51s %sSCRAPE OK%s' % (self.url, TerminalColors.OKGREEN, TerminalColors.ENDC))
        except Exception as e:
            output('Question:  %-51s %sSCRAPE FAILED%s' % (self.url, TerminalColors.FAIL, TerminalColors.ENDC),
                   'Error')
            output("Error '%s'" % e, 'Error')

    def add_to_db(self):
        global users_list, tags_list

        try:
            if self.user_id is not None:
                # If user not in DB, create it and add it to the DB!
                if self.user_id not in users_list:
                    # Creates the user and adds it to the list
                    user = User(self.user_id)
                    user.insert_to_db()
            else:
                self.user_id = 0

            # ADD QUESTION
            query = r"INSERT IGNORE INTO Questions(web_question_id, web_user_id, url, title, question_text, ask_date," \
                    r" weekly_parasha, nb_views, votes_count, nb_favorites, last_activity) " \
                    r"VALUES(%d, %d, '%s', '%s', '%s', '%s', %s, %d, %d, %d, '%s');" \
                    % (self.question_id, self.user_id, self.url, self.title.replace("'", r"\'"),
                       self.text.replace("'", r"\'"), self.ask_date,
                       "'%s'" % self.weekly_parasha.replace("'", r"\'") if self.weekly_parasha is not None else 'NULL',
                       self.view_count, self.vote_count, self.favorite_count, self.last_activity)
            run_query(query, True)

            with suppress(Exception):
                add_tags_to_db_if_missing(self.tags)

                # ADD QUESTIONTAGS Table
                for tag in self.tags:
                    query = r"INSERT INTO QuestionsTags(tag_label, question_web_id) " \
                            "VALUES('%s', %d);" % (tag, self.question_id)
                    run_query(query, True)

            # ADD ANSWERS
            for answer in self.answers:
                answer.add_to_db()

        except Exception as e:
            output("Error in Question.add_to_db() for ID: %d - '%s'" % (self.question_id, e), 'Error')


class Answer(object):
    def __init__(self, answer_soup, question_id):
        try:
            # Scrape container
            user_info = answer_soup.find_all('div', class_='user-info')[-1]

            # Scrape Answer's info
            self.answer_id = int(answer_soup['data-answerid'])
            self.question_id = question_id
            self.text = answer_soup.find('div', class_='post-text').get_text().strip()

            self.answer_date = None
            with suppress(AttributeError):
                self.answer_date = user_info.find('div', class_='user-action-time').contents[1]['title'][:-1]
            self.vote_count = int(answer_soup.find('span', class_='vote-count-post').get_text())
            self.accepted = not answer_soup.find('span', class_='vote-accepted-on') is None

            # Scrape original answerer's ID - 0 is id of 'unknown user'
            self.user_id = 0
            with suppress(AttributeError):
                user_url = DOMAIN_NAME + user_info.find('div', class_='user-details').find('a').attrs['href']
                self.user_id = int(re.search('[\w.-]+/users/([0-9]+)/.*', user_url).group(1))

            output('  Answer:  %-6s from Question(%s) %24s %sSCRAPE OK%s'
                   % (self.answer_id, self.question_id, '', TerminalColors.OKGREEN, TerminalColors.ENDC))
        except Exception as e:
            output('  Answer:  %-6s from Question(%s) %24s %sSCRAPE FAILED%s'
                   % (self.answer_id, self.question_id, '', TerminalColors.FAIL, TerminalColors.ENDC), 'Error')
            output("Error '%s'" % e, 'Error')

    def add_to_db(self):
        try:
            if self.user_id is not None:
                # If user not in DB, create it and add it to the DB!
                if self.user_id not in users_list:
                    # Creates the user and adds it to the list
                    user = User(self.user_id)
                    user.insert_to_db()
            else:
                self.user_id = 0

            # ADD ANSWER
            query = r"INSERT INTO Answers(web_answer_id, web_question_id, web_user_id, " \
                    "answer_text, answer_date, votes_count, accepted) " \
                    "VALUES(%d, %d, %d, '%s', %s, %d, %d);" % \
                    (self.answer_id, self.question_id, self.user_id, self.text.replace("'", r"\'"),
                     "'%s'" % self.answer_date if self.answer_date is not None else 'NULL',
                     self.vote_count, self.accepted)

            run_query(query, True)
        except Exception as e:
            output("Error in Answer.add_to_db() for question ID: %d - '%s'" % (self.question_id, e), 'Error')


class User(object):
    def __init__(self, user_id):
        global users_list

        # Update the users list
        users_list.append(user_id)

        self.user_id = user_id
        self.url = USER_PREFIX + str(user_id)

        try:
            # Scrape the containers
            user_details_soup = get_webpage_source(self.url)
            user_stats_container = user_details_soup.find('div', class_='grid--cell fl-shrink0 pr24')
            stats_list = user_stats_container.find_all('div', class_='grid--cell')

            # User Details
            self.username = user_details_soup.find('h2', class_='profile-user--name') \
                .find('div', class_='grid--cell').get_text()
            self.is_moderator = not user_details_soup.find('h2', class_='profile-user--name')\
                .find('span', class_='mod-flair') is None
            self.intro = user_details_soup.find('div', {'class': 'profile-user--bio'}).get_text().strip()
            self.picture_url = user_details_soup.find('img', class_='avatar-user')['src']

            self.location = ''
            with suppress(AttributeError):
                self.location = user_details_soup.find('svg', class_='iconLocation').parent.find_next_sibling(
                    'div').get_text().replace("'", r"\'")

            self.registration_date = user_details_soup.find('svg', class_='iconHistory') \
                .parent.find_next_sibling('div').find('span')['title'][:-1]

            # User Stats
            self.n_answers = int(stats_list[1].get_text().replace(',', ''))
            self.n_questions = int(stats_list[4].get_text().replace(',', ''))

            people_reach_str = stats_list[7].get_text().replace('~', '')
            if people_reach_str.isnumeric():
                self.people_reached = int(people_reach_str)
            else:
                self.people_reached = int(
                    float(people_reach_str[:-1]) * int('1' + people_reach_str[-1:].replace('k', '000')
                                                       .replace('m', '000000')))
            self.profile_views = int(user_details_soup.find('svg', class_='iconEye').parent
                                     .find_next_sibling('div').get_text().split()[0].replace(',', ''))

            # Achievements
            reputation_text = user_details_soup.find('div', {'title': 'reputation'}) \
                .find('div', class_='grid--cell').get_text()
            self.reputation = int(reputation_text.replace(',', ''))

            self.gold_badges = 0
            self.silver_badges = 0
            self.bronze_badges = 0

            with suppress(AttributeError):
                self.gold_badges = int(user_details_soup.find('span', class_='grid--cell badge1')
                                       .find_next_sibling('span').get_text())

            with suppress(AttributeError):
                self.silver_badges = int(user_details_soup.find('span', class_='grid--cell badge2')
                                         .find_next_sibling('span').get_text())
            with suppress(AttributeError):
                self.bronze_badges = int(user_details_soup.find('span', class_='grid--cell badge3')
                                         .find_next_sibling('span').get_text())

            output('User    :  %-51s %sSCRAPE OK%s' % (self.url, TerminalColors.OKGREEN, TerminalColors.ENDC))
        except Exception as e:
            output('User    :  %-51s %sSCRAPE FAILED%s' % (self.url, TerminalColors.FAIL, TerminalColors.ENDC),
                   'Error')
            output("Error '%s'" % e, 'Error')

    def insert_to_db(self):
        try:
            # ADD USER
            query = "INSERT INTO Users(web_user_id, url, name, is_moderator, intro, picture_url, location, " \
                    "registration_date, nb_answers, nb_questions, nb_people_reached, nb_profile_view, reputation," \
                    "nb_gold_badges, nb_silver_badges, nb_bronze_badges) " \
                    "VALUES(%d, '%s', '%s', %d, '%s', '%s', '%s', '%s', %d, %d, %d, %d, %d, %d, %d, %d);" % \
                    (self.user_id, self.url, self.username.replace("'", r"\'"), self.is_moderator,
                     self.intro.replace("'", r"\'"), self.picture_url, self.location, self.registration_date,
                     self.n_answers, self.n_questions, self.people_reached, self.profile_views, self.reputation,
                     self.gold_badges, self.silver_badges, self.bronze_badges)

            run_query(query, True)
        except Exception as e:
            output("Error in User.add_to_db() for user ID: %d - '%s'" % (self.user_id, e), 'Error')


def get_webpage_source(url_to_scrape):
    """Takes a URL input and returns the html code as a BS object"""
    time.sleep(SCRAPE_INTERVAL)
    html_file = urlopen(url_to_scrape)
    page_html = html_file.read()
    html_file.close()

    page_soup = Soup(page_html, "html.parser")

    return page_soup


def scrape_last_question_page_number(first_page_soup):
    """Returns the last question page number (useful when scraping from last page first)"""
    page_selection_footer = first_page_soup.find('div', {'class': 'pager fl'})

    return int(page_selection_footer.find('span', class_='page-numbers dots').find_next_sibling('a').get_text())


def question_url_generator(reverse_page_order, start_from_pages):
    """Generator function that returns the next page's address"""
    global last_question_page

    if reverse_page_order:
        current_page = last_question_page - start_from_pages + 1
        page_increment = -1
    else:
        current_page = start_from_pages
        page_increment = 1

    while last_question_page >= current_page >= 1:
        yield QUESTIONS_URL + '&page=' + str(current_page)
        current_page += page_increment


def scrape_questions_from_n_pages(n_pages, reverse_page_order, start_from_page):
    """Scrape all questions from n pages"""
    # Create Generator object
    question_generator = question_url_generator(reverse_page_order, start_from_page)

    for iteration in range(n_pages):
        # Use generator to get url of next page to scrape
        try:
            url_to_scrape = next(question_generator)

            if url_to_scrape == '':
                break

            # Get current page code
            current_page_soup = get_webpage_source(url_to_scrape)

            # Scrap question data from page
            output('Scraping page %3d/%-3d: %s' % (iteration + 1, n_pages, url_to_scrape))
            scrape_all_questions_in_page(current_page_soup)

            if reverse_page_order:
                with open(lastscrapefile, 'w') as f:
                    f.write(str(iteration + start_from_page))

        except StopIteration:
            output('Reached last available page. Exit.')
            exit(0)


def scrape_all_questions_in_page(question_page_soup):
    """The page is crunched and question objects are created and returned
    into a list"""
    global questions_list
    # output(question_page_soup.prettify())

    # Parse all question summaries in the page
    all_question_summary = question_page_soup.find_all('div', class_='question-summary')

    # For each question parse all data and create corresponding object
    for summary_container in all_question_summary:
        try:
            # Scrape the question id
            question_id = int(re.sub('question-summary-*', '', summary_container['id']))

            if question_id not in questions_list:
                # Create new question and add it to the list
                question = Question(question_id)

                # Add question to DB, as well as any dependant info (user, tag, answers)
                if question is not None:
                    question.add_to_db()

        except (TypeError, AttributeError) as e:
            output('ERROR in scrape_all_questions_in_page: %s' % e)


def add_tags_to_db_if_missing(tags_from_question):
    """Add any missing tag"""
    global tags_list

    for tag in tags_from_question:
        if tag not in tags_list:
            query = r"INSERT IGNORE INTO Tags(Label) Values('%s')" % tag
            run_query(query, True)
            tags_list.append(tag)


def fill_parasha_mapping():
    """Reads parasha mapping file and uploads values to db"""
    global parashiot_list
    df = pd.read_csv(FILE_PATH_MAP, delimiter=";", header=None, index_col=None)
    df = df.fillna("none")
    for index, row in df.iterrows():
        query = r'INSERT IGNORE INTO Parasha(parasha_name, taglabel) VALUES("%s", "%s");' % (row[0], row[1])
        run_query(query, True)
        parashiot_list.append(row[0])


def list_parashiot_from_api():
    greg_date = datetime.datetime.strptime('2018-11-29', "%Y-%m-%d").date()

    # Create multiprocessing pool
    pool = multiprocessing.Pool(processes=4)
    parashiot = pool.map(parashat_hashavua, [greg_date - timedelta(days=7 * i) for i in range(500)])
    pool.close()
    pool.join()

    print(set(parashiot))


def parashat_hashavua(greg_date: Union[datetime.datetime, str]):
    """Either takes date as a string parameter with format 'YYYY-MM-DD', or a date object
    and returns the corresponding parasha from the API"""
    if type(greg_date) is str:
        greg_date_new = re.match("([\w\W]*)\s", greg_date).group(1)
        greg_date = datetime.datetime.strptime(greg_date_new, "%Y-%m-%d").date()

    date_url = API_URL_TEMPLATE
    # Update url fields according to date
    date_url = date_url.replace('%d', greg_date.strftime("%d"))
    date_url = date_url.replace('%m', greg_date.strftime("%m"))
    date_url = date_url.replace('%Y', greg_date.strftime("%Y"))

    response = urlopen(date_url)

    try:
        parasha = json.loads(response.read().decode("utf-8"))['events'][0]
        # print('%s - %s' % (greg_date.strftime("%d-%m-%Y"), parasha if parasha is not None else 'No Parasha'))
    except KeyError:
        parasha = ''

    return parasha


def last_page_scrapped():
    try:
        with open(lastscrapefile, 'r') as f:
            last_scraped = f.read()
    except FileNotFoundError:
        last_scraped = 1

    return int(last_scraped)


@click.command()
# N_PAGES_TO_SCRAPE
@click.option('--pages', '-p', prompt='How many pages do you want to scrape ?',
              help='0 to scrape everything')
# START_FROM_PAGE
@click.option('--first', '-f', prompt='From which page do you want to scrape ?',
              help='Fill the number of the first page you want to scrape.\n '
                   'If_reverse true, first will be the nth from last page.\n '
                   '0 to resume from the last page scraped (if reverse is true)')
# START_FROM_OLDEST
@click.option('--reverse', '-r', default=False, prompt='Do you want to scrap from the end ? (0 or 1)',
              help='If you want to start the scrapping from the end -- False by default.')
def main(pages, first, reverse):
    """Welcome to "My Yodea Project", a little project to scrape questions from https://judaism.stackexchange.com/ """
    global logging_enabled, last_question_page

    # First Activate Logging
    if logging_enabled:
        logging.basicConfig(filename=LOGFILE, level=logging.INFO)

    # 1. Create DB if not exists
    sql_create_db_if_doesnt_exist()

    # 2. Get list of questions, users and tags from db in order not to scrape twice the same object
    get_last_data_from_db()

    # 3. Scrape the main question page
    main_page_soup = get_webpage_source(QUESTIONS_URL)

    # 4. Scrape the index of the last question page
    last_question_page = scrape_last_question_page_number(main_page_soup)

    # 5. Scrape questions and store in DB
    first_page_to_scrape = int(first) if first != '0' or not reverse else last_page_scrapped()
    n_pages_to_scrape = int(pages) if int(pages) > 0 else last_question_page-first_page_to_scrape+1
    scrape_questions_from_n_pages(n_pages_to_scrape, reverse, first_page_to_scrape)


if __name__ == "__main__":
    main()
