import miyodeascrape
import io
import os
from contextlib import redirect_stdout

import logging

DEV_LOGFILE = 'scrape_dev.log'


def confirm_function_ok(function_name):
    """Displays nicely that the function executed OK"""
    print('Tested %-28s -  %sOK%s' % (function_name + '()', miyodeascrape.TerminalColors.OKGREEN,
                              miyodeascrape.TerminalColors.ENDC))


def main():
    """Main function from the testing module"""
    # 1. Initiate tests
    logging.basicConfig(filename=DEV_LOGFILE, level=logging.INFO)
    miyodeascrape.enable_dev_env()

    # 2. Test Output method - Redirect std.out an compare it with test_val
    test_val = 'This is a test'
    stream = io.StringIO()
    with redirect_stdout(stream):
        miyodeascrape.output(test_val)

    output = stream.getvalue()
    with open(DEV_LOGFILE) as file:
        assert(file.read().find(test_val) >= 0)
        assert(output.find(test_val) == 0)

    os.remove(DEV_LOGFILE)
    confirm_function_ok('Output')
    miyodeascrape.printing_enabled = False

    # 3. Test Run Query
    output = miyodeascrape.run_query('SHOW DATABASES;')
    assert(('information_schema',) in output)
    confirm_function_ok('Query')

    # 4. Test SQL db creation - first delete the database if it exists
    miyodeascrape.run_query('DROP DATABASE IF EXISTS %s;' % miyodeascrape.database, True)
    miyodeascrape.sql_create_db_if_doesnt_exist()
    assert(len(miyodeascrape.run_query('SHOW TABLES')) == 6)
    confirm_function_ok('run_query')

    # 5. fill_parasha_mapping()
    miyodeascrape.fill_parasha_mapping()
    assert(miyodeascrape.run_query('SELECT count(*) Parasha')[0][0] > 0)
    confirm_function_ok('fill_parasha_mapping')

    # 6. Question() / Answer() / User() / add_tags_to_db_if_missing()
    print('****** Scraping Question 12760 ******')
    miyodeascrape.Question(12760).add_to_db()
    assert(miyodeascrape.run_query('SELECT count(*) FROM Questions;')[0][0] == 1)
    confirm_function_ok('Question')
    confirm_function_ok('Question.add_to_db')

    assert(miyodeascrape.run_query('SELECT count(*) FROM Answers;')[0][0] >= 1)
    confirm_function_ok('Answer')
    confirm_function_ok('Answer.add_to_db')

    assert (miyodeascrape.run_query('SELECT count(*) FROM Users;')[0][0] >= 1)
    confirm_function_ok('User')
    confirm_function_ok('User.add_to_db')

    assert (miyodeascrape.run_query('SELECT count(*) FROM Tags;')[0][0] >= 1)
    confirm_function_ok('add_tags_to_db_if_missing')

    # 8. get_last_data_from_db()
    miyodeascrape.get_last_data_from_db()
    assert(len(miyodeascrape.questions_list) == 1)
    assert(len(miyodeascrape.users_list) >= 1)
    assert(len(miyodeascrape.tags_list) >= 1)

    # 9. get_webpage_source(url_to_scrape)


    # 10. scrape_last_question_page_number(first_page_soup)
    # 11. question_url_generator(reverse_page_order, start_from_pages)
    # 12. scrape_questions_from_n_pages(n_pages, reverse_page_order, start_from_page)
    # 13. scrape_all_questions_in_page(question_page_soup)
    # 14. add_tags_to_db_if_missing(tags_from_question)
    # 15. fill_parasha_mapping()
    # 16. list_parashiot_from_api()
    # 17. parashat_hashavua(greg_date: Union[datetime.datetime, str])
    # 18. last_page_scrapped()
    # 19. main(pages, first, reverse)


    # assert()
    pass


if __name__ == "__main__":
    main()

